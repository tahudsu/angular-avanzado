import { Usuario } from './../models/usuario.model';
import { UsuarioService } from './../services/usuario/usuario.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

declare function init_plugins();
declare const gapi: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  rememberme = false;
  email: string;
  auth2: any;

  constructor(public router: Router,
    public _usuaruiService: UsuarioService) { }

  ngOnInit() {
    init_plugins();
    this.googleInit();
    this.email = localStorage.getItem('email') || '';

    if (this.email.length > 1) {
      this.rememberme = true;
    }
  }

  googleInit() {
    // https://developers.google.com/identity/sign-in/web/listeners
    gapi.load('auth2', () => {
      this.auth2 = gapi.auth2.init({
        clientid : '842588614193-skfppphr4k2nkfvn1c0k08fflsuufpbn.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });

      this.attachSignin(document.getElementById('btnGoogle'));
    });
  }

  attachSignin(element) {
    this.auth2.attachClickHandler(element, {}, googleUser => {
      // const profile = googleUser.getBasicProfile();
      // console.log(profile);
      const token = googleUser.getAuthResponse().id_token;
      console.log(token);
      this._usuaruiService.loginGoogle(token).subscribe(resp => {
        this.router.navigate(['/dashboard']);
      });
    });
  }

  ingresar(forma: NgForm) {

    if (forma.valid) {
      const usuario = new Usuario(null, forma.value.email, forma.value.password);
      this._usuaruiService.login(usuario, forma.value.rememberme).subscribe(() => {
        this.router.navigate(['/dashboard']);
      });
    }
  }
}
