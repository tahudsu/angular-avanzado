import { Usuario } from './../../models/usuario.model';
import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario/usuario.service';
import swal from 'sweetalert';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styles: []
})
export class ProfileComponent implements OnInit {
  usuario: Usuario;
  imagenSubir: File;
  imagenTemp: string | ArrayBuffer;

  constructor(public _usuarioService: UsuarioService) {
    this.usuario = this._usuarioService.usuario;
  }

  ngOnInit() {}

  guardar(usuario: Usuario) {
    this.usuario.nombre = usuario.nombre;
    if (!this.usuario.google) {
      this.usuario.email = usuario.email;
      this._usuarioService.updateUsuario(this.usuario).subscribe(resp => {
        console.log(resp);
      });
    }
  }

  seleccionImage(event) {
    console.log(event);
    if (event.target.files[0]) {
      if (!event.target.files[0].type) {
        this.imagenSubir = null;

        swal('el archivo seleccionado no es una imagen', 'error');
        return;

      }
      this.imagenSubir = event.target.files[0];

      const reader = new FileReader();
      const urlImagenTemp = reader.readAsDataURL(this.imagenSubir);
      reader.onloadend = () => {
        console.log(reader.result);
        this.imagenTemp = reader.result;
      }

    } else {
      this.imagenSubir = null;
      return;
    }
  }

  cambiarImagen() {
    console.log(this.imagenSubir);
    this._usuarioService.cambiarImagen(this.imagenSubir, this.usuario._id);
  }
}
