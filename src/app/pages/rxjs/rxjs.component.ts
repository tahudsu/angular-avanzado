import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { retry, map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: []
})
export class RxjsComponent implements OnInit, OnDestroy {
  subscription: Subscription;

  constructor() {

    this.subscription = this.regresaObservable()
      .subscribe(numero => console.log(numero),
        error => console.error(error),
        () => console.log('finished observer')
      );
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscription.unsubscribe();
  }

  regresaObservable(): Observable<any> {
    return new Observable(subscriber => {
      let contador = 0;

      const intervalo = setInterval(() => {
        contador += 1;

        const salida = {
          valor: contador
        }

        subscriber.next(salida);

        if (contador === 3) {
          // clearInterval(intervalo);
          // subscriber.complete();
        }

        /* if (contador === 2) {
          subscriber.error('error');
          clearInterval(intervalo);
        } */
      }, 1000);
    }).pipe(
      map((resp: any) => resp.valor),
      filter((valor, index) => {
        // console.log('filter', valor, index);
        if (( valor % 2) === 1) {
          return true;
        }

        return false;
      })
    );
  }

}
