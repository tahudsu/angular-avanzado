import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert';
import { SubirArchivoService } from 'src/app/services/service.index';
import { ModalUploadService } from './modal-upload.service';

@Component({
  selector: 'app-modal-upload',
  templateUrl: './modal-upload.component.html',
  styles: []
})
export class ModalUploadComponent implements OnInit {

  imagenSubir: File;
  imagenTemp: string | ArrayBuffer;
  constructor(
    protected _subirArchivosService: SubirArchivoService,
    public _modalUploadService: ModalUploadService
  ) { }

  ngOnInit() {
  }

  seleccionImage(event) {
    console.log(event);
    if (event.target.files[0]) {
      if (!event.target.files[0].type) {
        this.imagenSubir = null;

        swal('el archivo seleccionado no es una imagen', 'error');
        return;

      }
      this.imagenSubir = event.target.files[0];

      const reader = new FileReader();
      const urlImagenTemp = reader.readAsDataURL(this.imagenSubir);
      reader.onloadend = () => {
        console.log(reader.result);
        this.imagenTemp = reader.result;
      }

    } else {
      this.imagenSubir = null;
      return;
    }
  }

  subirImagen() {
    this._subirArchivosService.subirArchivo(this.imagenSubir, this._modalUploadService.tipo, this._modalUploadService.id)
      .then(res => {
        this._modalUploadService.notificacion.emit(res);
        this.cerrarModal();
      })
      .catch(err => {
        console.log('Error en la carga', err);
      });
  }

  cerrarModal() {
    this.imagenTemp = null;
    this.imagenSubir = null;
    this._modalUploadService.ocultarModal();
  }

}
