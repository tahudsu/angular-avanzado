import { LoginGuardGuard } from './guards/login-guard.guard';
import { UsuarioService } from './usuario/usuario.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsService,
  SidebarService,
  SharedService,
  HospitalService,
  MedicoService,
  AdminGuard,
  RenewTokenGuard } from 'src/app/services/service.index';
import { HttpClientModule } from '@angular/common/http';
import { SubirArchivoService } from './subir-archivo/subir-archivo.service';
import { ModalUploadService } from '../components/modal-upload/modal-upload.service';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  providers: [
    SettingsService,
    SidebarService,
    SharedService,
    UsuarioService,
    SubirArchivoService,
    LoginGuardGuard,
    ModalUploadService,
    HospitalService,
    MedicoService,
    AdminGuard,
    RenewTokenGuard
  ]
})
export class ServiceModule { }
