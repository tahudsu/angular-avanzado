import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { URL_SERVICIOS } from './../../config/config';
import { Injectable } from '@angular/core';
import { Usuario } from 'src/app/models/usuario.model';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert';
import { SubirArchivoService } from '../subir-archivo/subir-archivo.service';
import { of, throwError, pipe } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  usuario: Usuario;
  token: string;
  menu: any = [];
  constructor(public http: HttpClient,
    public _subirArchivoService: SubirArchivoService,
    public router: Router) { 
      this.loadStorage();
    }

  renewToken() {
    const url = URL_SERVICIOS + '/login/renewToken?token=' + this.token;

    return this.http.get(url)
      .pipe(map((res: any) => {
        this.token = res.token;
        localStorage.setItem('token', this.token);

        return true;
      }), catchError((err) => {
        swal('No se pudo renovar token', 'No fue posible renovar token', 'error');
        this.router.navigate(['/login']);
        return throwError(err);
      }));
  }

  loadStorage() {
    if (localStorage.getItem('token')) {
      this.token = localStorage.getItem('token');
      this.usuario = JSON.parse(localStorage.getItem('usuario'));
      this.menu = JSON.parse(localStorage.getItem('menu'));
    } else {
      this.token = '';
      this.usuario = null;
      this.menu = [];
    }
  }

  saveToStorage(id: string, token: string, usuario: Usuario, menu: any) {
    localStorage.setItem('id', id);
    localStorage.setItem('token', token);
    localStorage.setItem('usuario', JSON.stringify(usuario));
    localStorage.setItem('menu', JSON.stringify(menu));

    this.usuario = usuario;
    this.token = token;
    this.menu = menu;
  }

  loginGoogle(token: string) {
    const url = URL_SERVICIOS + '/login/google';
    return this.http.post(url, { token }).pipe(
      map((resp: any) => {
        console.log(resp);
        this.saveToStorage(resp.id, resp.token, resp.body, resp.menu);
      })
    );
  }

  login(usuario: Usuario, rememberme: boolean = false) {
    const url = URL_SERVICIOS + `/login`;
    if (rememberme) {
      localStorage.setItem('email', usuario.email);
    } else {
      localStorage.removeItem('email');
    }

    return this.http.post(url, usuario).pipe(map((res: any) => {
      console.log(res);
      // localStorage.setItem('id', res.id);
      // localStorage.setItem('token', res.token);
      // localStorage.setItem('usuario', JSON.stringify(res.body));
      this.saveToStorage(res.id, res.token, res.body, res.menu);
    }),
    catchError(err => {
      console.log(err);
      swal('Error en el login', err.error.message, 'error');
      return throwError(err); })
    );
  }

  crearUsuario(usuario: Usuario) {
    const url = `${URL_SERVICIOS}/usuario`;

    return this.http.post(url, usuario).pipe(map((resp: any) => {
      console.log(resp);
      swal('Usuario creado', usuario.email, 'success');
      return resp.usuario;
    }),
    catchError(err => {
      console.log(err);
      swal(err.error.mensaje, err.error.errors.message, 'error');
      return throwError(err); })
    );
  }

  isAuthenticated(): boolean {
    return !!this.token;
  }

  logout() {
    this.usuario = null;
    this.token = '';
    this.menu = {};
    localStorage.removeItem('token');
    localStorage.removeItem('usuario');
    localStorage.removeItem('menu');

    this.router.navigate(['/login']);
  }

  updateUsuario(usuario: Usuario) {
    let url = URL_SERVICIOS + '/usuario/' + usuario._id;
    url += '?token=' + this.token;
    console.log(url);
    return this.http.put(url, usuario)
    .pipe(
      map((resp: any) => {
        const usuarioResponse: Usuario = resp.usuario;
        if (usuarioResponse._id === this.usuario._id) {
          this.saveToStorage(usuarioResponse._id, this.token, usuarioResponse, this.menu);
        }

        swal('Usuario actualizado', usuario.nombre, 'success');

        return true;
      }),
      catchError(err => {
        console.log(err);
        swal(err.error.mensaje, err.error.errors.message, 'error');
        return throwError(err); }));
  }

  cambiarImagen(file: File, id: string ) {
    this._subirArchivoService.subirArchivo(file, 'usuarios', id)
      .then((resp: any) => {
        console.log(resp);
        this.usuario.img = resp.usuario.img;
        swal('Imagen actualizada', this.usuario.nombre, 'success');
        this.saveToStorage(id, this.token, this.usuario, this.menu);
      })
      .catch(err => {
        console.log(err);
      });
  }

  cargarUsuarios(desde: number = 0, ) {
    const url = URL_SERVICIOS + '/usuario?from=' + desde;

    return this.http.get(url);
  }

  buscarUsuarios(termino: string) {
    const url = URL_SERVICIOS + '/busqueda/coleccion/usuarios/' + termino;
    return this.http.get(url)
      .pipe(map((res: any) => res.usuarios));
  }

  borrarUsuario(usuario: Usuario) {
    const url = URL_SERVICIOS + '/usuario/' + usuario._id + '?token=' + this.token;
    return this.http.delete(url).pipe(map(() => {
      swal('Usuario borrado', 'El usuario ha sido eliminado correctamente', 'success');
      return true;
    }));
  }
}
