import { map } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { URL_SERVICIOS } from "../../config/config";
import { UsuarioService } from "../usuario/usuario.service";
import swal from "sweetalert";
import { Medico } from "../../models/medico.model";
import { pipe } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class MedicoService {
  totalMedicos: number = 0;
  constructor(
    public http: HttpClient,
    public _usuarioService: UsuarioService
  ) {}

  cargarMedicos() {
    const url = URL_SERVICIOS + "/medico";
    return this.http.get(url).pipe(
      map((res: any) => {
        this.totalMedicos = res.count;
        return res.medico;
      })
    );
  }

  buscarMedicos(termino: string) {
    const url = URL_SERVICIOS + "/busqueda/coleccion/medicos/" + termino;
    return this.http.get(url).pipe(map((res: any) => res.medicos));
  }

  borrarMedico(id: string) {
    let url = URL_SERVICIOS + "/medico/" + id;
    url += "?token=" + this._usuarioService.token;

    return this.http.delete(url).pipe(
      map(res => {
        swal("Medico Borrado", "Medico borrado correctamente", "success");
        return res;
      })
    );
  }

  guardarMedico(medico: Medico) {
    let url = URL_SERVICIOS + "/medico";
    let method = '';
    if (medico._id) {
      url += "/" + medico._id;
      url += "?token=" + this._usuarioService.token;
      method = 'put';
    } else {
      url += "?token=" + this._usuarioService.token;
      method = 'post';
    }

    return this.http
      [method](url, { medico, usuario: this._usuarioService.usuario })
      .pipe(
        map((res: any) => {
          swal("Medico creado", medico.nombre, "success");
          return res.medico;
        })
      );
  }

  cargarMedico(id: string) {
    const url = URL_SERVICIOS + "/medico/" + id;
    return this.http.get(url).pipe(
      map((res: any) => {
        return res.medico;
      })
    );
  }
}
